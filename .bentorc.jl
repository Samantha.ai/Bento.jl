# Default bentorc

tui -> begin
  if !haskey(rootpath(), "Test 123")
    println("Adding test data")
    dict = Dict(
      :a => Dict(:b => 3, :c => [1,2]),
      :b => 1:10,
      :c => () -> rand(10),
      :d => ["lol", "dude"]
    )
    rootpath()["Test 123"] = dict
  end

  menu = UIWidget(Menu())

  arg1 = PathRef(["Test 123", "b"])
  arg2 = PathRef(["Test 123", "c"])
  #pw = UIWidget(PlotWidget(:line, arg1, arg2))

  board = UIWidget(BoardLayout([(menu, (1,1), (20,120))]))#, (pw, (21,1), (40,80))]))

  push!(tui, board)
end

