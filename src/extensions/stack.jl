struct Stack
  stack::Vector
end

function process_packet!(self::UIWidget{Stack}, pkt::UIPacket{ExtensionAction})
  stk = item(self)
  stack = stk.stack
  action = event(pkt).action
  arg = event(pkt).arg
  if action == :pop
    if length(stack) > 0
      reply!(pkt, self, ExtensionActionReply(pop!(stack)))
    else
      # TODO
    end
  elseif action == :push
    push!(stack, arg)
    # TODO: reply!(pkt, self, ExtensionActionReply(proc.exitcode))
  else
    # TODO
  end
end
