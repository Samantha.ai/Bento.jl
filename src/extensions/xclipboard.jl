struct XClipboard
  target::Symbol
end

tags(::XClipboard) = [:clipboard]

function extension_action!(xc::XClipboard, action, arg)
  # TODO: Use target
  # TODO: Allow get/set target
  if action == :get
    stdout, stdin, proc = readandwrite(`xclip -o -d :0`)
    wait(proc)
    str = String(readavailable(stdout))
    reply!(pkt, self, ExtensionActionReply(str))
  elseif action == :set
    stdout, stdin, proc = readandwrite(`xclip -i -d :0`)
    println(stdin, arg)
    flush(stdin)
    close(stdin)
    reply!(pkt, self, ExtensionActionReply(proc.exitcode))
  else
    # TODO
  end
end
