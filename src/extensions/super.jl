#= TODO
Map ObjectID -> UIWidget
Choose new widget location
Stack
XClipboard
=#

struct SuperExtension
  item_widget_map::IdDict{Any,Vector{UIWidget}}
  focused_widgets::Vector{UIWidget}
  stack::Vector
  kb::KeyBindings
  direct_input::Ref{Bool}
end
SuperExtension() = SuperExtension(
  IdDict{Any,Vector{UIWidget}}(),
  UIWidget[],
  [],
  KeyBindings(default_keybindings(SuperExtension)),
  Ref(false)
)

struct SuperWidget
  #= TODO
  Stack
  Keybinding legend
  =#
  #stack::StackWidget
end

# FIXME: A hack of sorts...
ExtensionManager() = ExtensionManager(SuperExtension())

# FIXME: Merge into _all_ default keybindings
default_keybindings(::Type{SuperExtension}) = KBDict(
  'x' => KBDict(
    'a' => :add_widget,
    'p' => KBDict(
      'l' => :plot_line,
      'b' => :plot_bar,
    ),
    'q' => :quit,
    '\\' => :pop_stack,
    '\n' => :give_focus,
  ),
)

process_packet!(ext::SuperExtension, self, pkt) =
  send!(widget_item(self).child, self, pkt)
process_packet!(ext::SuperExtension, self, pkt::UIPacket{<:ExtensionActionRequest}) =
  extension_action!(ext, self, pkt)
process_packet!(ext::SuperExtension, self, pkt::UIPacket{<:ExtensionSetBufferRequest}) =
  extension_setbuffer!(ext, self, pkt)
process_packet!(ext::SuperExtension, self, pkt::UIPacket{<:ExtensionGetBufferRequest}) =
  extension_getbuffer!(widget_item(self).em.extension, self, pkt)

function process_packet!(ext::SuperExtension, self, pkt::UIPacket{FocusTransfer})
  isempty(ext.focused_widgets) && return force_focused(ext, self)
  last(ext.focused_widgets) === pkt.from || return force_focused(ext, self)
  push!(ext.focused_widgets, event(pkt).target)
  ext.direct_input[] = needs_direct_input(widget_item(event(pkt).target))

  clear_state!(ext.kb)
  ext_kb = default_keybindings(SuperExtension)
  widget_kb = default_keybindings(typeof(widget_item(event(pkt).target)))
  ext.kb.bindings = overlay_keybindings(ext_kb, widget_kb, :focused)

  force_focused(ext, self)
end
function process_packet!(ext::SuperExtension, self, pkt::UIPacket{FocusDrop})
  isempty(ext.focused_widgets) && return force_focused(ext, self)
  last(ext.focused_widgets) === pkt.from || return force_focused(ext, self)
  pop!(ext.focused_widgets)
  ext.direct_input[] = false

  if !isempty(ext.focused_widgets)
    ext_kb = default_keybindings(SuperExtension)
    widget_kb = default_keybindings(typeof(widget_item(last(ext.focused_widgets))))
    ext.kb.bindings = overlay_keybindings(ext_kb, widget_kb, :focused)
  else
    ext.kb.bindings = default_keybindings(SuperExtension)
  end

  force_focused(ext, self)
end
function process_packet!(ext::SuperExtension, self, pkt::UIPacket{DirectInputRequest})
  isempty(ext.focused_widgets) && return force_focused(ext, self)
  last(ext.focused_widgets) === pkt.from || return force_focused(ext, self)
  ext.direct_input[] = true
  clear_state!(ext.kb)
  reply!(pkt, self, DirectInputGrant())

  force_focused(ext, self)
end
function force_focused(ext::SuperExtension, self::UIWidget{TerminalUI})
  if isempty(ext.focused_widgets)
    child = widget_item(self).child
    log_err("Focus empty, transferring to $child")
    push!(ext.focused_widgets, child)

    clear_state!(ext.kb)
    ext.direct_input[] = false
    ext_kb = default_keybindings(SuperExtension)
    widget_kb = default_keybindings(typeof(child))
    ext.kb.bindings = overlay_keybindings(ext_kb, widget_kb, :focused)

    focus_grant!(self, child)
  end
end

function extension_action!(ext::SuperExtension, self, pkt)
  evt = event(pkt)
  source = evt.source
  action = Val(evt.action)
  args = evt.args
  se_action(source, action, args, ext, self, pkt)
end
function extension_setbuffer!(ext::SuperExtension, self, pkt)
  evt = event(pkt)
  source = evt.source
  item = evt.item
  args = evt.args
  se_setbuffer(source, item, args, ext, self, pkt)
end
function extension_getbuffer!(ext::SuperExtension, self, pkt)
  evt = event(pkt)
  source = evt.source
  args = evt.args
  reply!(pkt, self, ExtensionGetBufferReply(se_getbuffer(source, args, ext, self, pkt)))
end
function extension_kb!(ext::SuperExtension, self, event)
  action = process_input(ext.kb, event)
  log_err("Action: $action")
  action === nothing && return
  if action isa KBStatusFail
    ext.direct_input[] && process_action!(last(ext.focused_widgets), action.cstate)
    return
  end
  action = action.rstate

  if first(action) isa KBOverlayName
    action = map(a->a isa Symbol ? Val(a) : a, action[2:end])
    log_err("Sending to focused widget: $action")
    log_err("Focused widget: $(typeof(last(ext.focused_widgets)))")
    process_action!(last(ext.focused_widgets), action)
  else
    log_err("Sending to super extension: $action")
    action = map(a->a isa Symbol ? Val(a) : a, action)
    process_action!(ext, self, action)
  end
end
function process_action!(ext::SuperExtension, self, action::Tuple{Val{:quit}})
  exit(0)
end
function process_action!(ext::SuperExtension, self, action::Tuple{Val{:add_widget}})
  length(ext.stack) > 0 || return
  pr = pop!(ext.stack)
  uiw = UIWidget(default_widget(getdata(pr)))
  send!(widget_item(self).child, self, AddWidgetRequest(uiw))
end
function process_action!(ext::SuperExtension, self, action::Tuple{Val{:pop_stack}})
  if length(ext.stack) > 0
    pop!(ext.stack)
  end
end
#= TODO: Remove this once auto-focus works
function process_action!(ext::SuperExtension, self, action::Tuple{Val{:give_focus}})
  isempty(ext.focused_widgets) || return
  focused = widget_item(self).child
  push!(ext.focused_widgets, focused)
  clear_state!(ext.kb)
  ext_kb = default_keybindings(SuperExtension)
  widget_kb = default_keybindings(typeof(widget_item(focused)))
  ext.kb.bindings = overlay_keybindings(ext_kb, widget_kb, :focused)
end
=#
function process_action!(ext::SuperExtension, self, action::Tuple{V}) where V<:Union{Val{:plot_line},Val{:plot_bar}}
  action = first(typeof(first(action)).parameters)
  length(ext.stack) >= 2 || return
  pr2 = pop!(ext.stack)
  pr1 = pop!(ext.stack)
  if action == :plot_line
    plot_type = :line
  elseif action == :plot_bar
    plot_type = :bar
  end
  uiw = UIWidget(PlotWidget(plot_type, pr1, pr2))
  send!(widget_item(self).child, self, AddWidgetRequest(uiw))
end
#=
if action isa KBStatusFail
  for child in widget_item(self).children
    # TODO: This sucks
    for char in action.cstate
      send!(child, self, InputEvent(StdioInput(), char))
    end
  end
else
  action = first(action.rstate)
  if action == :quit
    exit(0)
  elseif action == :add_widget
    length(ext.stack) > 0 || return
    pr = pop!(ext.stack)
    uiw = UIWidget(default_widget(getdata(pr)))
    send!.(widget_item(self).children, Ref(self), Ref(AddWidgetRequest(uiw)))
  elseif action == :pop_stack
    if length(ext.stack) > 0
      pop!(ext.stack)
    end
  elseif action in (:plot_line, :plot_bar)
    length(ext.stack) >= 2 || return
    pr2 = pop!(ext.stack)
    pr1 = pop!(ext.stack)
    if action == :plot_line
      plot_type = :line
    elseif action == :plot_bar
      plot_type = :bar
    end
    uiw = UIWidget(PlotWidget(plot_type, pr1, pr2))
    send!.(widget_item(self).children, Ref(self), Ref(AddWidgetRequest(uiw)))
  end
end
=#

function se_action(source, action::Val{:exit}, args, ext, self, pkt)
  log_err("Exiting on request from $(typeof(source))")
  exit()
end
function se_setbuffer(source, item::PathRef, args, ext, self, pkt)
  push!(ext.stack, item)
  log_err("Stack Bottom")
  for entry in ext.stack
    log_err(entry)
  end
  log_err("Stack Top")
end
function se_getbuffer(source, args, ext, self, pkt)
  item = pop!(ext.stack)
  log_err("Sending $item to $(typeof(source))")
  item
end
