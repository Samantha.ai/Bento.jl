export Textbox

mutable struct Textbox
  value::String
  cursor::Int
end
Textbox() = Textbox("", 1)

default_keybindings(::Type{Textbox}) = KBDict(
  '\b' => :backspace,
  '\x7f' => :backspace,
  '\e' => KBDict(
    '[' => KBDict('3' => KBDict(
      '~' => :delete,
    )),
  )
)

needs_direct_input(::Textbox) = true
function process_action!(self::UIWidget{Textbox}, action::Tuple{Val{:backspace}})
  txtbox = widget_item(self)
  # FIXME: Delete at cursor-1
  txtbox.value = txtbox.value[1:end-1]
  redraw!(self)
end
function process_action!(self::UIWidget{Textbox}, action::Tuple{Val{:delete}})
  txtbox = widget_item(self)
  # FIXME: Delete at cursor
  redraw!(self)
end
function process_action!(self::UIWidget{Textbox}, keys::Vector{Char})
  txtbox = widget_item(self)
  # FIXME: Insert at cursor
  txtbox.value *= replace(String(keys), "\n" => "\r\n")
  redraw!(self)
end
function sanitize_input(keys)
  iob = IOBuffer()
  idx = 1
  while idx <= length(keys)
    char = keys[idx]
    if Int(char) >= 32
      print(iob, char)
    end
    idx += 1
  end
  String(iob)
end

function process_packet!(self::UIWidget{Textbox}, pkt::UIPacket{DrawRequest})
  txtbox = widget_item(self)

  panel_size = event(pkt).panel_size
  io = PanelIO(panel_size...)
  syntax_highlight!(io, txtbox.value)
  #print(io, "\e[5m\u2588\e[0m")
  print(io, "\u2588")
  panel = Panel(io)
  reply!(pkt, self, DrawReply(panel); close=false)
end
