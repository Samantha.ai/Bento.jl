export TerminalUI
export set_child!, start_ui

mutable struct TerminalUI
  child::Union{UIWidget,Nothing}
  kb::KeyBindings
  em::ExtensionManager
end
TerminalUI() = TerminalUI(nothing, KeyBindings(default_keybindings(TerminalUI)), ExtensionManager())
default_keybindings(::Type{TerminalUI}) = KBDict()

function set_child!(self::UIWidget{TerminalUI}, child)
  widget_item(self).child = child
  focus_transfer!(self, child)
  send!(child, self, DrawRequest(displaysize(stdout)))
end

function start_ui(self::UIWidget{TerminalUI})
  tui = item(self)

  # Send InitEvent
  # TODO: Move this to be an initializer for any newly-created widget
  #send!(tui.child, self, InitEvent())

  # Focus child
  send!(tui.child, self, FocusGrant())

  # STDIN input task
  @async begin
    si = StdioInput()
    while true
      if BENTO_QUIET[]
        event = get_event(si)
        extension_kb!(tui.em.extension, self, event)
      else
        try
          event = get_event(si)
          extension_kb!(tui.em.extension, self, event)
        catch err
          log_err("Error in TerminalUI stdin task")
          showerror(stdout, err); println()
          Base.show_backtrace(stdout, catch_backtrace()); println()
          # TODO: Waiting until exception stacks are backported
          #for (e,bt) in catch_stack()
          #  showerror(stdout, e, bt); println()
          #end
          #log_err(err)
          #log_err(stacktrace())
          #showerror(BENTO_LOGFILE, err)
          #rethrow(err)
        end
      end
    end
  end

  # Periodic draw task
  @async begin
    #=
    last_size = displaysize(stdout)
    while true
      cur_size = displaysize(stdout)
      if cur_size != last_size
        # FIXME: Composite/focus/etc.
        send_packet!(tui.children[1], self, DrawRequest(cur_size); replyto=[self])
        last_size = cur_size
      end
      sleep(0.1)
    end
    =#
  end

  # Initial draw
  send!(tui.child, self, DrawRequest(displaysize(stdout)))
end
process_packet!(self::UIWidget{TerminalUI}, pkt) =
  process_packet!(widget_item(self).em.extension, self, pkt)
process_packet!(self::UIWidget{TerminalUI}, pkt::UIPacket{RedrawEvent}) =
  send!(widget_item(self).child, self, DrawRequest(displaysize(stdout)))
function process_packet!(self::UIWidget{TerminalUI}, pkt::UIPacket{DrawReply})
  panel = event(pkt).panel
  panel_size = displaysize(stdout)
  panel = resize_panel(panel, panel_size)
  # TODO: Encompass more stuff?
  if !BENTO_QUIET[]
    print(stdout, panel)
  end
end
