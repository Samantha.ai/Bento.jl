module Bento

### Imports ###

using BentoBase
import BentoBase: default_widget, widget_group, default_pathref, default_style, default_layout, default_plot, default_label, LayoutStyle, PlotStyle
using Crayons
using VT100
import VT100: ScreenEmulator, Cell
using UnicodePlots
using JLD2
import InteractiveUtils: edit
using Pkg, UUIDs

export load_bentorc, init_bento

include("paths.jl")
include("buffers.jl")
include("color-themes.jl")
include("highlighting.jl")
include("packets.jl")
include("widgets.jl")
include("focus.jl")
include("timers.jl")
include("panel.jl")
include("render.jl")
include("keybindings.jl")
include("extensions.jl")
include("terminalui.jl")
include("compositor.jl")
include("layouts.jl")
include("editors.jl")
include("textbox.jl")
include("menu.jl")
include("misc.jl")
include("plotting.jl")
include("save-load.jl")
include("defaults.jl")

include("widgets/label.jl")
include("widgets/checkbox.jl")
include("widgets/stack.jl")
include("extensions/super.jl")

function log_err(x)
  if BENTO_LOGFILE[] != nothing
    println(BENTO_LOGFILE[], x)
    flush(BENTO_LOGFILE[])
  end
end

function load_bentorc()
  # Load .bentorc.jl, if it exists
  bentorc = nothing
  cwd = pwd()
  while realpath(cwd) != "/"
    if isfile(joinpath(cwd, ".bentorc.jl"))
      bentorc = joinpath(cwd, ".bentorc.jl")
      break
    end
    cwd = joinpath(cwd, "..")
  end
  if bentorc == nothing
    if isfile(joinpath(homedir(), ".bentorc.jl"))
      bentorc = joinpath(homedir(), ".bentorc.jl")
    elseif isfile(joinpath(homedir(), ".config", "bento", "bentorc.jl"))
      bentorc = joinpath(homedir(), ".config", "bento", "bentorc.jl")
    end
  end
  if bentorc != nothing
    println("Loading bentorc from $(abspath(bentorc))")
    Pkg.API.activate(abspath(splitdir(bentorc)[1]))
    func = Base.include(Main, bentorc)
    if func isa Function
      return func
    end
  end
  return tui -> ()
end

global const BENTO_LOGFILE = Ref{Union{IOStream,Nothing}}()
global const BENTO_WORKSPACE = Ref{String}("")
global const BENTO_WORKSPACE_AUTOSAVE = Ref(true)
global const BENTO_QUIET = Ref(false)

function init_bento(;quiet=false, bare=false)
  # Open logfile if requested
  if haskey(ENV, "BENTO_LOGFILE")
    BENTO_LOGFILE[] = open(ENV["BENTO_LOGFILE"], "a+")
    atexit() do
      close(BENTO_LOGFILE[])
    end
  else
    BENTO_LOGFILE[] = nothing
  end
  log_err("-----Init Bento-----")

  # Get workspace path
  if !bare
    BENTO_WORKSPACE[] = get(ENV, "BENTO_WORKSPACE", joinpath(homedir(), ".bento-workspace"))
    mkpath(BENTO_WORKSPACE[])

    BENTO_WORKSPACE_AUTOSAVE[] = true
  end

  BENTO_QUIET[] = quiet
  if !BENTO_QUIET[]
    ccall(:jl_exit_on_sigint, Nothing, (Cint,), 0)
    Crayons.force_color(true)
    run(`stty isig -icanon -echo min 1`)
    print("\e]0;Bento\e\\")
    print("\e[?25l")
    #print("\e[?47h")
    atexit() do
      run(`stty cooked echo`)
      print("\e[?25h")
      #print("\e[?47l")
      println()
    end
  end
end

end
