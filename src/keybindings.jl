export KeyBindings, KBDict, KBParseNumber

abstract type AbstractKBCommand end
const KBDict = Dict{Char,Union{Dict,Symbol,AbstractKBCommand}}

# TODO: Allow arbitrary functions as well?
mutable struct KeyBindings
  bindings::KBDict
  mode::Symbol
  mode_idx::Int
  cstate::Vector{Char}
  rstate::Vector
  current
end
KeyBindings(bindings=KBDict()) = KeyBindings(bindings, :default, 0, Char[], [], nothing)

struct KBParseNumber <: AbstractKBCommand
  bindings::KBDict
end
KBParseNumber() = KBParseNumber(KBDict())

struct KBOverlay{B} <: AbstractKBCommand
  name::Symbol
  bindings::B
end

struct KBOverlayName
  name::Symbol
end

# TODO: KBParseUntil
# TODO: KBParseStop

struct KBStatusSuccess{T<:Tuple}
  rstate::T
end
struct KBStatusFail
  cstate::Vector{Char}
end

struct DirectInputRequest <: UIEvent end
struct DirectInputGrant <: UIEvent end
struct DirectInputRevoke <: UIEvent end

needs_direct_input(x) = false

process_input(kb::KeyBindings, event::InputEvent{<:Any,<:Char}) =
  process_input(kb, event.value)
function process_input(kb::KeyBindings, char::Char)
  # Get to current if necessary
  if kb.current == nothing
    kb.current = kb.bindings
    for (idx,key) in enumerate(kb.cstate)
      status, current = _process_next(kb, kb.current, key; idx=idx)
      if status && current == nothing
        clear_state!(kb)
        return nothing
      end
      @assert !status "Match terminated before processing newest char"
      kb.current = current
    end
  end

  push!(kb.cstate, char)
  status, current = _process_next(kb, kb.current, char; idx=length(kb.cstate))
  if status isa Bool
    rstate = copy(kb.rstate)
    cstate = copy(kb.cstate)
    clear_state!(kb)
    return status ? KBStatusSuccess(tuple(rstate...)) : KBStatusFail(cstate)
  else
    kb.current = current
    return nothing
  end
end

function clear_state!(kb::KeyBindings)
  empty!(kb.cstate)
  empty!(kb.rstate)
  kb.mode_idx = 0
  kb.current = nothing
end

# TODO: Support mapping symbol key (:left, :enter, etc.) to char via user-defined dispatch
function _process_next(kb::KeyBindings, current::Dict, char::Char; idx=0)
  if haskey(current, char)
    if current[char] isa Dict
      return (nothing, current[char])
    elseif current[char] isa KBParseNumber
      kb.mode = :number
      kb.mode_idx = idx+1
      push!(kb.rstate, 0)
      return (nothing, current[char])
    elseif current[char] isa KBOverlay
      push!(kb.rstate, KBOverlayName(current[char].name))
      # FIXME: Pull out this check into a function (to test for termination)
      if current[char].bindings isa Symbol
        push!(kb.rstate, current[char].bindings)
        return (true, current[char].bindings)
      else
        return (nothing, current[char].bindings)
      end
    else
      push!(kb.rstate, current[char])
      return (true, current[char])
    end
  else
    push!(kb.rstate, char)
    return (false, char)
  end
end

function _process_next(kb::KeyBindings, current::KBParseNumber, char::Char; idx=0)
  ex = Meta.parse(String(kb.cstate[kb.mode_idx:end]); raise=false)
  result = try
    eval(ex)
  catch
    nothing
  end
  if result isa Number
    pop!(kb.rstate)
    push!(kb.rstate, result)
    return (nothing, current)
  else
    kb.mode = :default
    kb.mode_idx = 0
    return _process_next(kb, current.bindings, char)
  end
end

function overlay_keybindings(lower_kb::Dict, upper_kb::Dict, upper_name::Symbol)
  merged_kb = KBDict()
  merged_keys = union(keys(lower_kb), keys(upper_kb))
  for key in merged_keys
    if haskey(lower_kb, key) && haskey(upper_kb, key)
      @assert (lower_kb[key] isa Dict) && (upper_kb[key] isa Dict) "lower_kb and upper_kb have non-Dict conflict"
      merged_kb[key] = overlay_keybindings(lower_kb[key], upper_kb[key], upper_name)
    elseif haskey(lower_kb, key) && !haskey(upper_kb, key)
      merged_kb[key] = lower_kb[key]
    else
      merged_kb[key] = KBOverlay(upper_name, upper_kb[key])
    end
  end
  merged_kb
end
