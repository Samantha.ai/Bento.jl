using Crayons

struct ColorTheme{name}
  colors::Dict
end
solarized_dark = ColorTheme{:solarized_dark}(Dict(
  "yellow"  => 0xb58900,
  "orange"  => 0xcb4b16,
  "red"     => 0xdc322f,
  "magenta" => 0xd33682,
  "violet"  => 0x6c71c4,
  "blue"    => 0x268bd2,
  "cyan"    => 0x2aa198,
  "green"   => 0x859900,

  "base03"  => 0x002b36,
  "base02"  => 0x073642,
  "base01"  => 0x586e75,
  "base00"  => 0x657b83,
  "base0"   => 0x839496,
  "base1"   => 0x93a1a1,
  "base2"   => 0xeee8d5,
  "base3"   => 0xfdf6e3,
))

function color_idx(theme::ColorTheme{:solarized_dark}, idx)
  fg_colors = ["red", "orange", "yellow", "green", "cyan", "blue", "violet", "magenta"]
  return (theme.colors[fg_colors[((idx-1)%length(fg_colors))+1]], theme.colors["base02"])
end
