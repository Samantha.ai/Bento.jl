export UIWidget
export item, event, set_default_widget!, _default_widget

abstract type UIEvent end
struct InitEvent <: UIEvent end
struct ExitEvent <: UIEvent end

abstract type InputSource end
struct InputEvent{IS<:InputSource,V} <: UIEvent
  source::IS
  value::V
end
struct StdioInput <: InputSource end
get_event(si::StdioInput) = InputEvent(si, Char(read(stdin, 1)[1]))

struct UIPacket{E<:UIEvent}
  from
  forwards::Vector
  event::E
  session::UUID
end
# FIXME: Use incrementing counter for session?
UIPacket(from, forwards, event) = UIPacket(from, forwards, event, uuid4())

event(pkt::UIPacket) = pkt.event
