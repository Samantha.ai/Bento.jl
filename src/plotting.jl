export PlotWidget

mutable struct PlotWidget{K}
  plot_type::Symbol
  args::Tuple
  kwargs::K
  plot_obj
  cache::Panel
end
PlotWidget(plot_type, args...; kwargs...) =
  PlotWidget(plot_type, args, kwargs, nothing, Panel(undef,1,1))

# FIXME: List out all supported plot types, instead of pirating the default function
default_plot(style::PlotStyle{plot_type}, args...; kwargs...) where plot_type =
  PlotWidget(plot_type, args...; kwargs...)

Base.show(io::IO, pw::PlotWidget) = print(io, "PlotWidget($(pw.plot_type), $(pw.args...))")
function process_packet!(self::UIWidget{<:PlotWidget}, pkt::UIPacket{DrawRequest})
  pw = item(self)
  panel_size = event(pkt).panel_size
  if size(pw.cache) != panel_size
    draw_plot!(pw, panel_size)
  end
  panel = pw.cache
  reply!(pkt, self, DrawReply(panel))
end
function draw_plot!(pw::PlotWidget, panel_size=size(pw.cache))
  args = map(arg->arg isa PathRef ? getdata(arg) : arg, pw.args)
  args = map(arg->arg isa Function ? arg() : arg, args)
  kwargs = Dict{Symbol,Any}(pw.kwargs)
  kwargs[:height] = panel_size[1]-2
  kwargs[:width] = panel_size[2]-2
  if pw.plot_type == :line
    pw.plot_obj = lineplot(args...; kwargs...)
  elseif pw.plot_type == :bar
    pw.plot_obj = barplot(args...; kwargs...)
  elseif pw.plot_type == :stairs
    pw.plot_obj = strairsplot(args...; kwargs...)
  elseif pw.plot_type == :histogram
    pw.plot_obj = histogram(args...; kwargs...)
  elseif pw.plot_type == :spy
    pw.plot_obj = spy(args...; kwargs...)
  elseif pw.plot_type == :heatmap
    # FIXME: pw.plot_obj = heatmap!
  end
  # FIXME: Render labels manually
  io = PanelIO(panel_size...)
  print(io, pw.plot_obj.graphics)
  pw.cache = Panel(io)
end
function process_packet!(self::UIWidget{<:PlotWidget}, pkt::UIPacket{TimerEvent})
  pw = item(self)
  draw_plot!(pw)
end


# TODO: Color scale
function heatmap!(io::IO, arr::Matrix)
  amin, amax = minimum(arr), maximum(arr)
  for j in axes(arr, 2)
    for i in axes(arr, 1)
      val = (arr[i,j]-amin)/(amax-amin)
      didx = round(Int, (val*4)+1)
      print(io, UnicodePlots.den_signs[didx])
    end
    println(io)
  end
  io
end

function f()
  uc = rand(0:255, 3)
  lc = rand(0:255, 3)
  return "\e[38;2;$(uc[1]);$(uc[2]);$(uc[3])m\e[48;2;$(lc[1]);$(lc[2]);$(lc[3])m$(Char(0x2580))"
end

function g(io::IO, rows, cols)
  for j in 1:rows
    for i in 1:cols
      print(io, f())
    end
    println(io, "\e[0m")
  end
end
