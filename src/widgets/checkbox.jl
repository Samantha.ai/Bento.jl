struct CheckboxWidget{PR<:AbstractPathRef}
  pr::PR
  kb::KeyBindings
end
CheckboxWidget(pr) =
  CheckboxWidget(pr, KeyBindings(default_keybindings(CheckboxWidget)))

default_keybindings(::Type{CheckboxWidget}) = KBDict(
  '\n' => :toggle,
  'y' => :on,
  'n' => :off,
)

function process_packet!(self::UIWidget{C} where C<:CheckboxWidget, pkt::UIPacket{DrawRequest})
  cbox = widget_item(self)

  panel_size = event(pkt).panel_size
  io = PanelIO(panel_size...)
  value = getdata(cbox.pr)::Bool
  print(io, value ? 'Y' : 'N') # FIXME: Char(0x2714) : Char(0x2717))
  panel = Panel(io)
  reply!(pkt, self, DrawReply(panel))
end

function process_packet!(self::UIWidget{C} where C<:CheckboxWidget, pkt::UIPacket{<:InputEvent})
  cbox = widget_item(self)

  action = process_input(cbox.kb, event(pkt))
  (action === nothing || action isa KBStatusFail) && return
  action = first(action.rstate)
  if action == :toggle
    setdata!(cbox.pr, !getdata(cbox.pr))
  elseif action == :on
    setdata!(cbox.pr, true)
  elseif action == :off
    setdata!(cbox.pr, false)
  else
    return
  end
  send!(self, RedrawEvent())
end
