struct StackWidget
  stack::Vector
end
StackWidget() = StackWidget([])

function process_packet!(self::UIWidget{StackWidget}, pkt::UIPacket{DrawRequest})
  stack = widget_item(self).stack

  panel_size = event(pkt).panel_size
  io = PanelIO(panel_size...)
  
  printpanel(io, "Top"; height=1)#, fill='-', align=:centered)
  printpanel(io, map(repr, reverse(stack)); height=1)
  printpanel(io, "Bottom"; height=1)#, fill='-', align=:centered)

  panel = Panel(io)
  reply!(self, pkt, DrawReply(panel))
end
