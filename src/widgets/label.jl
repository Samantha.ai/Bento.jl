struct LabelWidget{PR<:AbstractPathRef}
  pr::PR
end

function process_packet!(self::UIWidget{L} where L<:LabelWidget, pkt::UIPacket{DrawRequest})
  label = widget_item(self)

  panel_size = event(pkt).panel_size
  io = PanelIO(panel_size...)
  str = getdata(label.pr)::String
  print(io, str)
  panel = Panel(io)
  reply!(pkt, self, DrawReply(panel))
end

default_label(x::A where A<:AbstractPathRef) = LabelWidget(x)
