struct UIHook{F<:Function,C<:Channel}
  func::F
  chan::C
end

struct UIWidget{T}
  item::T
  # TODO: Change to Channel{UIPacket}
  inbox::Vector{UIPacket}
  replybox::Vector{UIPacket}
  mappings::Dict{Int,Int}
  hooks::Vector{UIHook}
  doorbell::Condition
  task::Task
end
function UIWidget(item)
  inbox = UIPacket[]
  replybox = UIPacket[]
  mappings = Dict{Int,Int}()
  hooks = UIHook[]
  doorbell = Condition()
  tmpChan = Channel(1)
  task = Task(() -> begin
    self = take!(tmpChan)
    while true
      while length(inbox) > 0
        packet = popfirst!(inbox)
        # Test hooks
        fallback = true
        for (idx,hook) in enumerate(hooks)
          if hook.func(packet)
            put!(hook.chan, packet)
            deleteat!(hooks, idx)
            fallback = false
            break
          end
        end

        # Fallback
        fallback && _process_packet!(self, packet)
      end
      wait(doorbell)
    end
  end)
  uiw = UIWidget(item, inbox, replybox, mappings, hooks, doorbell, task)
  schedule(task)
  put!(tmpChan, uiw)
  uiw
end

# Some convenience conversions
Base.convert(::Type{UIWidget}, x) = UIWidget(x)
Base.convert(::Type{UIWidget}, x::UIWidget) = x

Base.show(io::IO, widget::UIWidget{T}) where T =
  print(io, "UIWidget{$T}")

struct WidgetGroup{I}
  cpathref::RelativePathRef{I,P,T} where {P,T}
  widget::UIWidget
end

_process_packet!(self, pkt) = @async begin
  if BENTO_QUIET[]
    process_packet!(self, pkt)
  else
    try
      process_packet!(self, pkt)
    catch err
      log_err("Error in $(typeof(self)) when processing packet $(typeof(pkt))")
      exit(0)
      showerror(stdout, err); println()
      Base.show_backtrace(stdout, catch_backtrace()); println()
      # TODO: Waiting until exception stacks are backported
      #for (e,bt) in catch_stack()
      #  showerror(stdout, e, bt); println()
      #end
      #log_err(err)
      #log_err(stacktrace())
      #showerror(BENTO_LOGFILE, err)
      #rethrow(err)
    end
  end
end

"""
Waits for the hook to trigger, and returns the matching packet
"""
function waithook!(func::Function, self::UIWidget; chanType=Any)
  chan = Channel{chanType}(1)
  push!(self.hooks, UIHook(func, chan))
  take!(chan)
end

"""
Sends a packet, and waits for a reply to return
"""
function send_recv!(self, pkt, reply_type)
  send!(self, pkt)
  waithook!(self) do reply_pkt
    event(reply_pkt) isa reply_type
  end
end

# Choose appropriate widget for supplied type
_default_widget(obj) = UIWidget(default_widget(obj))
_default_widget() = UIWidget(default_widget())

item(uiw::UIWidget) = uiw.item
widget_item(uiw::W where W<:UIWidget) = uiw.item

"""
Saves a packet to the replybox for later retrieval
"""
save!(self::UIWidget, pkt::UIPacket) =
  push!(self.replybox, pkt)

function load!(self::UIWidget, pkt::UIPacket; close=true)
  orig_session = self.mappings[pkt.session]
  orig_pkt = first(filter(p->p.session==orig_session, self.replybox))
  close && delete!(self.mappings, pkt.session)
  orig_pkt
end

function send!(target, self, event; forwards=UIWidget[], link=nothing)
  pkt = UIPacket(self, forwards, event)
  push!(target.inbox, pkt)
  notify(target.doorbell)
  if link != nothing
    self.mappings[pkt.session] = link.session
    log_err("$(link.session): Linked to $(pkt.session) by $(typeof(self))")
  end
  log_err("$(pkt.session): $(typeof(self)) sent $(typeof(pkt.event)) to $(typeof(target))")
end

function reply!(orig_pkt::UIPacket{E} where E, self::UIWidget{I} where I, event; close=true)
  target, forwards = next_forward(orig_pkt)
  if target != nothing
    pkt = UIPacket(self, forwards, event, orig_pkt.session)
    push!(target.inbox, pkt)
    notify(target.doorbell)
    log_err("$(pkt.session): $(typeof(self)) forwarded $(typeof(pkt.event)) to $(typeof(target))")
  else
    target = orig_pkt.from
    pkt = UIPacket(self, forwards, event, orig_pkt.session)
    push!(target.inbox, pkt)
    notify(target.doorbell)
    log_err("$(pkt.session): $(typeof(self)) replied $(typeof(pkt.event)) to $(typeof(target))")
  end
  if close
    idx = findfirst(p->p.session==orig_pkt.session, self.replybox)
    idx !== nothing && deleteat!(self.replybox, idx)
  end
end

next_forward(pkt::UIPacket) = next_forward(pkt.forwards)
function next_forward(forwards::Vector{UIWidget})
  if length(forwards) >= 1
    target = shift!(forwards)
    return (target, forwards)
  else
    return (nothing, forwards)
  end
end

process_packet!(self, pkt) =
  log_err("Failed sending $(typeof(pkt.event)) to $(typeof(self))")

const DEFAULT_WIDGET = Ref{UIWidget{T} where T}()
set_default_widget!(default) =
  DEFAULT_WIDGET[] = default
send!(self, event; forwards=UIWidget[]) =
  send!(DEFAULT_WIDGET[], self, event; forwards=forwards)
