export AbstractPathRef, PathRef, RelativePathRef, AbstractPath, StaticPath, DynamicPath, UndefinedPath
export rootpath, temppath, lookup, fullpath
export name, parent, children, child, getdata, setdata!, deldata!, update!, get_idx

abstract type AbstractPath end
abstract type AbstractPathRef end

struct PathRef{P<:Tuple} <: AbstractPathRef
  path::P
end
PathRef(apath::AbstractPath) = PathRef(fullpath(apath))

struct RelativePathRef{X,P<:Tuple,I<:Tuple} <: AbstractPathRef
  item::X
  path::P
  idxs::I
end
RelativePathRef(x) = RelativePathRef(x, (), ())
# TODO: Alternative to splatting?
# FIXME: Use getfield dummy!!!
RelativePathRef(pathref::RelativePathRef, x::Ref) =
  RelativePathRef(getfield(pathref, :item), getfield(pathref, :path), (getfield(pathref, :idxs)..., x[]))

struct StringPathRef{P<:Tuple} <: AbstractPathRef
  pieces::P
end

#struct TempPathRef <: AbstractPathRef
#  path::
#end

mutable struct StaticPath <: AbstractPath
  name::String
  parent::Union{AbstractPath,Nothing}
  children::Vector{AbstractPath}
end
StaticPath(name::AbstractString, parent=nothing) = StaticPath(String(name), parent, AbstractPath[])
StaticPath(name::AbstractString, parent::PathRef) = StaticPath(String(name), lookup(parent), AbstractPath[])
# TODO: Cleanup
function StaticPath(def::Pair, parent=nothing)
  name = def[1]
  self = StaticPath(name, parent, [])
  if def isa Pair
    item = def[2]
    if item isa Union{Pair,String}
      push!(self.children, StaticPath(item, self))
    elseif item isa Union{Array,Tuple}
      for subitem in item
        if subitem isa Union{Pair,String}
          push!(self.children, StaticPath(subitem, self))
        else
          push!(self.children, subitem)
        end
      end
    else
      self.data = item
    end
  end
  return self
end

global const BENTO_ROOT_PATH = Ref{AbstractPath}(StaticPath("__ROOT__"))
global const BENTO_TEMP_PATH = Ref{AbstractPath}(StaticPath("__TEMP__"))

mutable struct DynamicPath{T} <: AbstractPath
  name::String
  data::T
  parent::Union{AbstractPath,Nothing}
  children::Vector{AbstractPath}
end
DynamicPath(name::AbstractString, data, parent=nothing; depth=1) =
  update!(DynamicPath(String(name), data, parent, AbstractPath[]); depth=depth)
DynamicPath(name::AbstractString, data, parent::PathRef; depth=1) =
  DynamicPath(String(name), data, lookup(parent); depth=depth)

abstract type AbstractMagicalPath <: AbstractPath end
# FIXME: Rename to DynamicPath :)
struct MagicalPath{T,P<:Union{AbstractMagicalPath,StaticPath},Idx} <: AbstractMagicalPath
  item::T
  parent::P
  index::Idx
end

# Note: Below, index is 0 because child's index could change, and we don't want to rely on this value being set to idx
MagicalPath(spath::StaticPath, idx::Int) =
  MagicalPath(getdata(spath, idx), spath, 0)

ismpathrooted(mpath::MagicalPath{T,P,Idx}) where {T,P<:StaticPath,Idx} = true
ismpathrooted(mpath::MagicalPath) = false

"""
Path indicating that a DynamicPath's child is not defined
"""
mutable struct UndefinedPath <: AbstractPath
  name::String
  parent::Union{AbstractPath,Nothing}
end

# TODO: SymbolicPath, ClusterManagerMount, FilesystemMount, NetworkMount


### Methods ###

rootpath() = Bento.BENTO_ROOT_PATH[]
temppath() = Bento.BENTO_TEMP_PATH[]

function fullpath(apath::AbstractPath)
  apath == rootpath() && return ()
  path = []
  current = apath
  while true
    if current == rootpath()
      break
    end
    pushfirst!(path, name(current))
    current = parent(current)
  end
  return tuple(path...)
end
#=function Base.show(io::IO, spath::StaticPath)
  println(io, typeof(spath), ": ", getdata(spath))
end=#
#=
function Base.showall(io::IO, spath::StaticPath; indent=0, offset=0)
  #=if indent != 0
    for i in 0:1:indent
      if i == offset
        print(io, "\u2514")
        for j in i:offset
          print(io, "-")
        end
        break
      else
        print(io, "|")
      end
    end
  end=#
  if spath == rootpath()
    name = "/"
    print(io, "\e[31m")
  else
    name = "/" * spath.name
    print(io, "\e[32m")
  end
  println(io, repeat(" ", indent), "$name: ", getdata(spath))
  for (idx,child) in enumerate(children(spath))
    showall(io, child; indent=indent+1, offset=idx-1)
  end
  print("\e[0m")
end
=#

function lookup(pathref::PathRef)
  path = rootpath()
  length(pathref.path) == 0 && return path
  for idx in 1:length(pathref.path)
    path = child(path, pathref.path[idx])
    path isa DynamicPath && update!(path; depth=length(pathref.path)-idx+1)
  end
  path
end
# FIXME: Lookup by Vector{String} instead
#=function lookup(path::AbstractString, spath::StaticPath=rootpath(), force=false, value=nothing)
  tokens = split(strip(path, '/'), "/")
  for (tidx,token) in enumerate(tokens)
    token == "" && error("Empty")
    idx = findfirst(child->child.name==token, spath.children)
    if idx == 0
      if !force
        error("Token $token not found in path: $(fullpath(spath))")
      else
        spath = push!(spath.children, StaticPath(token, tidx==length(tokens)?value:nothing, spath))[end]
      end
    else
      spath = spath.children[idx]
    end
  end
  return spath
end=#

Base.getindex(pathref::RelativePathRef, idxs...) =
  RelativePathRef(pathref, Ref(idxs))
Base.getproperty(pathref::RelativePathRef, sym::Symbol) =
  RelativePathRef(pathref, Ref(sym))

name(pathref::PathRef) = name(lookup(pathref))
name(spath::StaticPath) = spath.name
name(dpath::DynamicPath) = dpath.name
name(udpath::UndefinedPath) = udpath.name

get_idx(pathref::PathRef) = get_idx(lookup(pathref))
get_idx(apath::AbstractPath) = findfirst(x->x==apath, children(parent(apath)))

function getdata(pathref::RelativePathRef)
  current = getfield(pathref, :item)
  for idx in getfield(pathref, :idxs)
    if idx isa Symbol
      current = getproperty(current, idx)
    else
      current = getindex(current, idx...)
    end
  end
  current
end
function getdata(pathref::StringPathRef)
  iob = IOBuffer()
  for piece in pathref.pieces
    if piece isa String
      print(iob, piece)
    elseif piece isa AbstractPathRef
      print(iob, String(getdata(piece)))
    else
      error("Unknown piece type $(typeof(piece))")
    end
  end
  String(iob.data)
end
  
getdata(pathref::PathRef) = getdata(lookup(pathref))
getdata(spath::StaticPath) = nothing
getdata(dpath::DynamicPath) = dpath.data
getdata(udpath::UndefinedPath) = nothing

function setdata!(pathref::RelativePathRef, value)
  current = getfield(pathref, :item)
  idxs = getfield(pathref, :idxs)
  @assert length(idxs) > 0 "Cannot modify root of RelativePathRef"
  for (_idx, idx) in enumerate(idxs)
    if _idx == length(idxs)
      last_idx = last(idxs)
      @info last_idx
      if last_idx isa Symbol
        setproperty!(current, last_idx, value)
      else
        setindex!(current, value, last_idx...)
      end
      return
    else
      if idx isa Symbol
        current = getproperty(current, idx)
      else
        current = getindex(current, idx...)
      end
    end
  end
  current
end
setdata!(pathref::PathRef, value, idx) = setdata!(lookup(pathref), value, idx)
# FIXME: Don't do a .data here!
function setdata!(dpath::DynamicPath, value, idx)
  child(dpath, idx) isa UndefinedPath && return value
  # FIXME: Check for type match
  _setdata!(dpath, value, name(child(dpath, idx)))
  value
end

_setdata!(dpath::DynamicPath{T}, value, field) where T =
  setfield!(dpath.data, Symbol(field), value)
_setdata!(dpath::DynamicPath{T}, value, key) where T<:Dict{K,V} where {K,V} =
  setindex!(dpath.data, convert(V, value), convert(K, key))
_setdata!(dpath::DynamicPath{T}, value, idx) where T<:Vector{E} where E =
  setindex!(dpath.data, convert(E, value), parse(idx))

deldata!(pathref::PathRef, idx) =
  deldata!(lookup(pathref), idx)
deldata!(spath::StaticPath, idx) =
  deleteat!(children(spath), idx)
deldata!(dpath::DynamicPath{T}, key) where T<:Dict =
  delete!(dpath.data, collect(keys(dpath.data))[key])#; update!(dpath))
deldata!(dpath::DynamicPath{T}, idx) where T<:Vector =
  deleteat!(dpath.data, idx)

update!(pathref::PathRef; kwargs...) = update!(lookup(pathref); kwargs...)
update!(apath::AbstractPath; kwargs...) = ()
function update!(dpath::DynamicPath; depth=1) #, ptrs=Str{Ptr{Nothing}}())
  # TODO: Is this the correct location for the check & return?
  #ptr = pointer_from_objref(dpath.data)
  #ptr in ptrs && return dpath
  #push!(ptrs, pointer_from_objref(dpath.data))
  depth == 0 && return dpath

  empty!(dpath.children)
  getchildren!(dpath; depth=depth-1)
  dpath
end
function getchildren!(dpath::DynamicPath{T}; depth=1) where T
  for field in fieldnames(typeof(dpath.data))
    if isdefined(dpath.data, field)
      push!(dpath, DynamicPath(string(field), getfield(dpath.data, field), dpath; depth=depth))
    else
      push!(dpath, UndefinedPath(string(field), dpath))
    end
  end
end
function getchildren!(dpath::DynamicPath{T}; depth=1) where T<:Dict
  for key in keys(dpath.data)
    push!(dpath, DynamicPath(string(key), dpath.data[key], dpath; depth=depth))
  end
end
function getchildren!(dpath::DynamicPath{T}; depth=1) where T<:Vector
  for idx in 1:length(dpath.data)
    if isassigned(dpath.data, idx)
      push!(dpath, DynamicPath(string(idx), dpath.data[idx], dpath; depth=depth))
    else
      push!(dpath, UndefinedPath(string(idx), dpath))
    end
  end
end
getchildren!(dpath::DynamicPath{T}; depth=1) where T<:Type = ()

parent(pathref::PathRef) = parent(lookup(pathref))
parent(spath::StaticPath) = spath.parent
parent(dpath::DynamicPath) = dpath.parent
parent(udpath::UndefinedPath) = udpath.parent

children(pathref::PathRef) = children(lookup(pathref))
children(spath::StaticPath) = spath.children
children(dpath::DynamicPath) = dpath.children
children(udpath::UndefinedPath) = []

child(pathref::PathRef, idx) = child(lookup(pathref), idx)
child(spath::StaticPath, idx::Int) = spath.children[idx]
function child(spath::StaticPath, name::String)
  idx = findfirst(c->c.name==name, spath.children)
  @assert idx > 0 "No such child: $name"
  return spath.children[idx]
end
child(dpath::DynamicPath, idx::Int) = dpath.children[idx]
function child(dpath::DynamicPath, name::String)
  idx = findfirst(c->c.name==name, dpath.children)
  @assert idx > 0 "No such child: $name"
  return dpath.children[idx]
end
child(udpath::UndefinedPath, idx) = nothing

Base.convert(::Type{PathRef}, apath::AbstractPath) = PathRef(fullpath(apath))

Base.length(apath::AbstractPath) = length(children(apath))
Base.length(pathref::PathRef) = length(lookup(pathref))
Base.getindex(spath::StaticPath, path) = child(spath, path)
Base.getindex(spath::StaticPath, next_path, paths...) = child(spath, path)[paths...]
# FIXME: Base.setindex!(apath::AbstractPath, value, path::Vararg{AbstractString})
function Base.setindex!(apath::AbstractPath, value, path::AbstractString)
  push!(apath, DynamicPath(path, value, apath))
end
function Base.push!(parent::AbstractPath, child::AbstractPath)
  push!(children(parent), child)
  child.parent = parent
end
Base.push!(parent::AbstractPath, child) = push!(parent.children, child)
Base.push!(pathref::PathRef, child) = push!(lookup(pathref), child)

Base.:*(pathref::PathRef, next) = PathRef(vcat(pathref.path, string(next)))
Base.haskey(apath::AbstractPath, key::String) =
  any(c->name(c)==key, children(apath))
