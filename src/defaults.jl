default_pathref(x) =
  RelativePathRef(x)

default_layout(::LayoutStyle{:default}, x...) =
  BoardLayout(UIWidget.([x...]))
default_layout(::LayoutStyle{:split_vertical}, x...) =
  SplitLayout{:vertical}([x...])
default_layout(::LayoutStyle{:split_horizontal}, x...) =
  SplitLayout{:horizontal}([x...])

default_style(uiw::UIWidget{W} where W) =
  default_style(widget_item(uiw))

BentoBase._default_widget(pr, ::Type{Bool}) =
  CheckboxWidget(pr)

widget_group(pr, widget) = WidgetGroup(pr, UIWidget(widget))
function process_packet!(self::UIWidget{<:WidgetGroup}, pkt::UIPacket{DrawRequest})
  send!(widget_item(self).widget, self, event(pkt))
  reply_pkt = waithook!(self) do reply_pkt
    event(reply_pkt) isa DrawReply
  end
  reply!(pkt, self, event(reply_pkt))
end

default_keybindings(uiw::UIWidget{W}) where W =
  default_keybindings(W)
default_keybindings(::Type{UIWidget{W}}) where W =
  default_keybindings(W)

# TODO: Need a better place for this, I feel
function default_widget(x)
  #x isa RelativePathRef && throw(1)
  # TODO: This is kinda weird, I should figure out how to make this work in isolation
  if x isa RelativePathRef
    pr = x
    x = getdata(pr)
  else
    pr = RelativePathRef(x)
  end
  BentoBase._default_widget(pr, typeof(x))
end
