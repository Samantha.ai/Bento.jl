#=
struct CompositedDrawRequest <: UIEvent
  panel_size::Tuple{Int,Int}
  requests::Vector{Tuple{UIWidget,Dict{Symbol,Any}}}
end
=#
struct Compositor
  panel::Panel
end
function Compositor(self::UIWidget{W} where W, dr::DrawRequest)
  panel_size = dr.panel_size
  defaults = merge(default_style(), default_style(self))

  # Draw background
  default_cell = Cell(Cell(' '); bg=defaults.background.color)
  if haskey(defaults, :background_pattern)
    if defaults.background_pattern isa Cell
      default_cell = defaults.background_pattern
      panel = fill(default_cell, panel_size...)
    else
      # FIXME: Draw pattern
    end
  else
    panel = fill(default_cell, panel_size...)
  end

  Compositor(panel)
end

Panel(compositor::Compositor) = compositor.panel

#function process_packet!(self::UIWidget{Compositor}, pkt::UIPacket{CompositedDrawRequest})
function composite!(compositor::Compositor, self::UIWidget{W} where W,
                    target::UIWidget{W} where W; params=Dict())
  # FIXME: The whole shebang
  panel_size = size(compositor.panel)

  #requests = sort(event(pkt).requests; by=req->get(req[2], :zorder, 0))
  # TODO: Run asynchronously
  #for req in requests
  #  params = req[2]
    subpanel_loc = params[:location]
    subpanel_size = params[:size]
    any(subpanel_size .+ subpanel_loc .- 1 .<= 0) && return
    any(subpanel_loc .> panel_size) && return

    # Shrink by border size
    # FIXME: Only shrink if borders requested
    subpanel_size_bordered = subpanel_size .- 2
    any(subpanel_size_bordered .< 1) && return

    # Request panel
    send!(target, self, DrawRequest(subpanel_size_bordered))
    reply_pkt = waithook!(self) do reply_pkt
      event(reply_pkt) isa DrawReply
    end
    subpanel_inner = event(reply_pkt).panel

    # Enlarge and reposition for borders
    subpanel = fill(Cell(' '), subpanel_size)
    dst_ci = CartesianIndices((2:size(subpanel,1)-1, 2:size(subpanel,2)-1))
    src_ci = CartesianIndices((1:subpanel_size[1]-2, 1:subpanel_size[2]-2))
    copyto!(
      subpanel,
      dst_ci,
      subpanel_inner,
      src_ci
    )

    # Apply styling
    subpanel = style_panel(subpanel, target)

    # Draw subpanel onto panel
    dst_ul = max.((1,1), subpanel_loc)
    dst_br = min.(panel_size, subpanel_size .+ subpanel_loc .- 1)
    dst_size = dst_br .- dst_ul .+ 1
    src_ul = dst_ul .- subpanel_loc .+ 1
    src_br = min.(subpanel_size, dst_br .- subpanel_loc .+ 1)
    src_ci = CartesianIndices((src_ul[1]:src_br[1], src_ul[2]:src_br[2]))
    dst_ci = CartesianIndices((dst_ul[1]:dst_br[1], dst_ul[2]:dst_br[2]))
    copyto!(compositor.panel, dst_ci, subpanel, src_ci)
  #end
end

canonicalize_color(color::Symbol) =
  (get(VT100.colorlist, color, nothing), nothing)
# TODO: color::RGB
# TODO: color::NTuple{3,Real}

canon_bg_merge(cell, color) =
  Cell(cell;
    bg=(color[1] !== nothing ? color[1] : cell.bg),
    bg_rgb=(color[2] !== nothing ? color[2] : cell.bg_rgb))

function style_panel(old_panel, origin)
  panel_size = size(old_panel)

  # TODO: Recursive merge?
  style = merge(default_style(), default_style(origin))
  new_panel = similar(old_panel)

  # FIXME: Perform background detection
  if style.background.autodetect
    for cell in old_panel
    end
  end

  # Apply element-wise styling
  bg_color_tuple = canonicalize_color(style.background.color)
  for j in axes(old_panel, 2)
    for i in axes(old_panel, 1)
      # Apply background color
      if style.background.force
        new_panel[i,j] = canon_bg_merge(old_panel[i,j], bg_color_tuple)
      end
    end
  end

  # Apply borders
  # TODO: Use style config
  # Top and Bottom
  new_panel[1,:] = fill(Cell('━'), panel_size[2])
  new_panel[end,:] = fill(Cell('━'), panel_size[2])
  # Left and Right
  new_panel[:,1] = fill(Cell('┃'), panel_size[1])
  new_panel[:,end] = fill(Cell('┃'), panel_size[1])
  # Top-Left
  new_panel[1,1] = Cell('┏')
  # Top-Right
  new_panel[1,end] = Cell('┓')
  # Bottom-Left
  new_panel[end,1] = Cell('┗')
  # Bottom-Right
  new_panel[end,end] = Cell('┛')

  return new_panel
end

function default_style()
  (
    zorder = 0,
    background = (
      autodetect = false,
      force = true,
      color = :black,
    ),
    border = (
      enabled = true,
      fg_color = :white,
      bg_color = :black,
      char = '#',
    ),
  )
end
