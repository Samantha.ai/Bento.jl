using OhMyREPL, Tokenize, Crayons

# TODO: Add some passes!

function syntax_highlight!(io::IO, str::String)
  tokens = collect(tokenize(str))
  crayons = fill(Crayon(), length(tokens))
  for pass in reverse(OhMyREPL.PASS_HANDLER.passes)
    pass[2].f!(crayons, tokens, 0)
  end
  iob = IOBuffer()
  OhMyREPL.untokenize_with_ANSI(iob, crayons, tokens)
  print(io, String(take!(iob)))
end
