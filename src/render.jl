struct DrawRequest <: UIEvent
  panel_size::Tuple{Int,Int}
end
struct DrawReply <: UIEvent
  panel::Panel
  position::Tuple{Int,Int}
end
DrawReply(panel) = DrawReply(panel, (0,0))
struct RedrawEvent <: UIEvent end

redraw!(self) = send!(self, RedrawEvent())

# FIXME: Support :displaysize and :position
# FIXME: Optimize for run-length encoding
function Base.show(io::IO, panel::Panel)
  print(io, "\e[1;1H")
  iob = IOBuffer()
  panel_dump!(iob, panel)
  print(io, String(take!(iob)))
end
function panel_dump!(iob::IOBuffer, panel::Panel)
  height, width = size(panel)
  for h in 1:height
    print(iob, "\e[s")
    for w in 1:width
      cell_dump!(iob, panel[h,w])
      print(iob, "\e[0m")
    end
    print(iob, "\e[u\e[B")
  end
end
function cell_dump!(iob::IOBuffer, cell::VT100.Cell)
  # FIXME: More attrs
  if cell.attrs & VT100.Attributes.Blink > 0
    print(iob, "\e[5m")
  end
  if cell.flags & VT100.Flags.FG_IS_RGB > 0
    r, g, b = Int(floor(cell.fg_rgb.r*255)), Int(floor(cell.fg_rgb.g*255)), Int(floor(cell.fg_rgb.b*255))
    print(iob, "\e[38;2;$(r);$(g);$(b)m")
  elseif cell.flags & VT100.Flags.FG_IS_256 > 0
    if 0 <= cell.fg <= 7
      print(iob, "\e[3$(string(cell.fg))m")
    elseif 8 <= cell.fg <= 15
      print(iob, "\e[9$(string(cell.fg-8))m")
    else
      print(iob, "\e[38;5;$(string(cell.fg))m")
    end
  else
    print(iob, "\e[39m")
  end

  if cell.flags & VT100.Flags.BG_IS_RGB > 0
    r, g, b = Int(floor(cell.bg_rgb.r*255)), Int(floor(cell.bg_rgb.g*255)), Int(floor(cell.bg_rgb.b*255))
    print(iob, "\e[48;2;$(r);$(g);$(b)m")
  elseif cell.flags & VT100.Flags.BG_IS_256 > 0
    if 0 <= cell.bg <= 7
      print(iob, "\e[4$(string(cell.bg))m")
    elseif 8 <= cell.bg <= 15
      print(iob, "\e[10$(string(cell.bg-8))m")
    else
      print(iob, "\e[48;5;$(string(cell.bg))m")
    end
  else
    print(iob, "\e[49m")
  end

  print(iob, cell.content)
end
