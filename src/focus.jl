struct FocusTransfer <: UIEvent
  target::UIWidget
end
struct FocusDrop <: UIEvent end
struct FocusGrant <: UIEvent end
struct FocusLoss <: UIEvent end
#struct FocusRevoke <: UIEvent end

focus_transfer!(self::UIWidget, target::UIWidget) =
  send!(self, FocusTransfer(target))
focus_drop!(self::UIWidget) = send!(self, FocusDrop())
focus_grant!(self::UIWidget, target::UIWidget) =
  send!(target, self, FocusGrant())
