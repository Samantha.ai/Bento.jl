#struct ExtensionQueryRequest <: UIEvent end
#struct ExtensionQueryReply <: UIEvent end
#struct ExtensionActionRequest <: UIEvent end
#struct ExtensionActionReply <: UIEvent end
#struct Extension
#  name::Symbol
#  data
#end
#Extension(name) = Extension(name, nothing)

struct ExtensionActionRequest{W,NT<:Union{NamedTuple,Nothing}} <: UIEvent
  source::UIWidget{W}
  action::Symbol
  args::NT
end
struct ExtensionSetBufferRequest{W,IT,NT<:Union{NamedTuple,Nothing}} <: UIEvent
  source::UIWidget{W}
  item::IT
  args::NT
end
struct ExtensionGetBufferRequest{W,IT,NT<:Union{NamedTuple,Nothing}} <: UIEvent
  source::UIWidget{W}
  args::NT
end
struct ExtensionGetBufferReply{IT} <: UIEvent
  item::IT
end

struct ExtensionManager{E}
  #extensions::Vector{Extension}
  extension::E
end
#ExtensionManager() = ExtensionManager(Extension[])

# TODO: Recursive getindex/setindex! on Extension data

#getindex(ext::Extension, path::Symbol) = getindex(ext.data, path)
#getindex(ext::Extension, first_path, paths...) = getindex(ext.data
# TODO: Default process_packet! handlers

# Core extension handler fallbacks
extension_action!(ext, self, pkt) =
  log_err("Failed to process ExtensionActionRequest via extension of type $(typeof(ext))")
extension_setbuffer!(ext, self, pkt) =
  log_err("Failed to process ExtensionSetBufferRequest via extension of type $(typeof(ext))")
extension_getbuffer!(ext, self, pkt) =
  log_err("Failed to process ExtensionGetBufferRequest via extension of type $(typeof(ext))")

# Convenience send! wrappers
ext_action!(self, action, args=nothing) =
  send!(self, ExtensionActionRequest(self, action, args))
ext_setbuffer!(self, item, args=nothing) =
  send!(self, ExtensionSetBufferRequest(self, item, args))
ext_getbuffer!(self, args=nothing) =
  send_recv!(self, ExtensionGetBufferRequest(self, args), ExtensionGetBufferReply)
