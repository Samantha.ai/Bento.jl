export Menu

mutable struct Menu
  focused_path::AbstractPathRef
  selected_idx::Int
  idx_mem::Vector{Int}
  txtbox::UIWidget{Textbox}
  txtmode::Bool
  txtstate::Symbol
  editors::Dict{Type,AbstractEditor}
  focused::Bool
  kb::KeyBindings
end
Menu(pathref::PathRef, kb) =
  Menu(pathref,
       1,
       Int[],
       UIWidget(Textbox()),
       false,
       :none,
       Dict{Type,AbstractEditor}(Any => JuliaDefaultEditor(),),
       false,
       kb)
Menu(apath::AbstractPath, kb) = Menu(PathRef(fullpath(apath)), kb)
Menu() = Menu(rootpath(), KeyBindings(default_keybindings(Menu)))
default_keybindings(::Type{Menu}) = KBDict(
  'y' => :copy,
  'p' => :paste,
  'i' => :insert_static,
  'I' => :insert_dynamic,
  '\b' => :undo,
  '\e' => KBDict(
    '\e' => :unfocus,
    '[' => KBDict(
      'A' => :up,
      'B' => :down,
      'C' => :child,
      'D' => :parent,
      '2' => KBDict(
        '~' => :insert_static
      ),
      '3' => KBDict(
        '~' => :delete
      )
    )
  )
)

function process_packet!(self::UIWidget{Menu}, pkt::UIPacket{<:InputEvent})
  menu = widget_item(self)

  # Process event
  if menu.txtmode
    return send!(menu.txtbox, self, event(pkt))
  end
  action = process_input(menu.kb, event(pkt))
  (action === nothing || action isa KBStatusFail) && return
  action = first(action.rstate)
  redraw = true
  if action in (:up, :down, :child, :parent)
    change_menu(menu, action)
  #=
  elseif action == :push_temp
    push!(menu.temp_store, getdata(child(menu.focused_path, menu.selected_idx)))
  elseif action == :pop_temp
    if length(menu.temp_store) > 0
      #setdata!(menu.focused_path, getdata(child(menu.temp_store[1], menu.temp_store[2])), menu.selected_idx)
      temp = pop!(menu.temp_store)
      #if temp isa Tuple{PathRef, Int}
      #  temp = getdata(child(temp[1], temp[2]))
      #end
      setdata!(menu.focused_path, temp, menu.selected_idx)
    else
      redraw = false
    end
  elseif action == :delete_temp
    if length(menu.temp_store) > 0
      pop!(menu.temp_store)
    else
      redraw = false
    end
  =#
  elseif action == :copy
    ext_setbuffer!(self, PathRef(child(menu.focused_path, menu.selected_idx)))
  elseif action == :paste
    item = ext_getbuffer!(self)
    setdata!(menu.focused_path, item, menu.selected_idx)
  elseif action == :execute
    menu_txtmode!(self, :execute)
    # TODO: Display result and save for later
  elseif action == :insert_static
    #name = doedit(menu.editors, "")
    #push!(menu.focused_path, StaticPath(name, menu.focused_path))
    menu_txtmode!(self, :insert_static)
  elseif action == :insert_dynamic
    name = doedit(menu.editors)
    result = doedit(menu.editors)
    push!(menu.focused_path, DynamicPath(name, result, menu.focused_path))
  elseif action == :replace
    if !isimmutable(getdata(menu.focused_path)) && menu.selected_idx > 0
      data = getdata(child(menu.focused_path, menu.selected_idx))
      data = doedit(menu.editors, data)
      try
        setdata!(menu.focused_path, data, menu.selected_idx)
      catch err
        log_err("Failed to replace: $err")
      end
    else
      redraw = false
    end
  elseif action == :delete
    if lookup(menu.focused_path) isa StaticPath
      deldata!(menu.focused_path, menu.selected_idx)
    elseif !isimmutable(getdata(menu.focused_path)) && menu.selected_idx > 0
      deldata!(menu.focused_path, menu.selected_idx)
    else
      redraw = false
    end
  elseif action == :txtbox
    menu_txtmode!(self, :demo)
  elseif action == :unfocus
    req_pkt = last(filter(p->p isa UIPacket{FocusGrant}, self.replybox))
    menu.focused = false
    reply!(req_pkt, self, FocusLoss())
    filter!(p->p isa UIPacket{FocusGrant}, self.replybox)
  elseif action == nothing
    redraw = false
  else
    log_err("Unknown Menu action: $action")
    redraw = false
  end
  if menu.selected_idx == 0 && length(menu.focused_path) > 0
    menu.selected_idx = 1
  elseif menu.selected_idx < 0
    menu.selected_idx = 0
  elseif menu.selected_idx > length(menu.focused_path)
    menu.selected_idx = length(menu.focused_path)
  end
  if redraw
    send!(self, RedrawEvent())
  end
end
function process_action!(self::UIWidget{Menu}, action::Tuple{V}) where V<:Union{Val{:up},Val{:down},Val{:child},Val{:parent}}
  menu = widget_item(self)
  action = first(typeof(first(action)).parameters)
  change_menu(menu, action)
  redraw!(self)
end
function process_action!(self::UIWidget{Menu}, action::Tuple{Val{:copy}})
  menu = widget_item(self)
  ext_setbuffer!(self, PathRef(child(menu.focused_path, menu.selected_idx)))
end
function process_action!(self::UIWidget{Menu}, action::Tuple{Val{:paste}})
  menu = widget_item(self)
  item = ext_getbuffer!(self)
  setdata!(menu.focused_path, item, menu.selected_idx)
end
function process_action!(self::UIWidget{Menu}, action::Tuple{Val{:unfocus}})
  menu = widget_item(self)
  focus_drop!(self)
  redraw!(self)
end

function process_packet!(self::UIWidget{Menu}, pkt::UIPacket{DrawRequest})
  menu = widget_item(self)

  if menu.txtmode
    send!(menu.txtbox, self, DrawRequest((1,event(pkt).panel_size[2])))
    reply_pkt = waithook!(self) do reply_pkt
      event(reply_pkt) isa DrawReply
    end
    panel_size = event(pkt).panel_size
    txtbox_panel = event(reply_pkt).panel
    panel = render(self, panel_size, txtbox_panel)
    reply!(pkt, self, DrawReply(panel))
  else
    panel_size = event(pkt).panel_size
    panel = render(self, panel_size)
    reply!(pkt, self, DrawReply(panel))
  end
end
function render(self::UIWidget{Menu}, panel_size, txtbox_panel=nothing)
  menu = widget_item(self)
  io = PanelIO(panel_size...)
  apath = lookup(menu.focused_path)
  println(io, apath.name, "  (", join(menu.focused_path.path, " -> "), ")")

  if txtbox_panel != nothing
    print(io, txtbox_panel[1,:])
    print(io, "\e[0m")
  end

  for (idx,item) in enumerate(children(apath))
    print(io, (idx == menu.selected_idx ? "=>" : "  ") * name(item))
    if getdata(item) != nothing
      print(io, ": ")
      syntax_highlight!(io, repr(getdata(item)))
    end
    print(io, "\n")
  end
  panel = Panel(io)
  #if menu.focused
  #  panel[end,end] = Cell(panel[end,end]; content='0', fg=:blue, bg=:red)
  #end
  return panel
end

function change_menu(menu::Menu, dir::Symbol)
  apath = lookup(menu.focused_path)
  idx = menu.selected_idx
  if dir == :parent
    apath.parent == nothing && return
    menu.focused_path = PathRef(parent(menu.focused_path))
    menu.selected_idx = length(menu.idx_mem) > 0 ? pop!(menu.idx_mem) : 1
  elseif dir == :child
    length(apath) == 0 && return
    menu.focused_path = PathRef(child(apath, idx))
    push!(menu.idx_mem, idx)
    menu.selected_idx = length(menu.focused_path) > 0 ? 1 : 0
  elseif dir == :up
    menu.selected_idx = (idx > 1 ? idx-1 : 1)
  elseif dir == :down
    menu.selected_idx = (idx < length(apath) ? idx+1 : length(apath))
  end
end

function menu_txtmode!(self::UIWidget{Menu}, state::Symbol)
  menu = widget_item(self)
  if state == :none
    menu.txtmode = false
  else
    menu.txtmode = true
    menu.focused = false
    send!(menu.txtbox, self, FocusGrant())
  end
  menu.txtstate = state
end
function process_packet!(self::UIWidget{Menu}, pkt::UIPacket{FocusGrant})
  menu = widget_item(self)
  menu.focused = true
  save!(self, pkt)
end
function process_packet!(self::UIWidget{Menu}, pkt::UIPacket{FocusLoss})
  menu = widget_item(self)
  @assert pkt.from == menu.txtbox
  if menu.txtstate == :execute
    str = widget_item(menu.txtbox).value
    result = Main.eval(parse(str))
    #push!(menu.temp_store, result)
  elseif menu.txtstate == :insert_static
    name = widget_item(menu.txtbox).value
    push!(menu.focused_path, StaticPath(name, menu.focused_path))
  end
  menu_txtmode!(self, :none)
end
