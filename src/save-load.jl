export save_workspace, load_workspace

# FIXME: Mark all new files/folders with current timestamp
# FIXME: Record pointers into toplevel JLD2 file
# FIXME: Check for pointers (and other non-serializable stuff)
function save_workspace()
  path = rootpath()
  cd(BENTO_WORKSPACE) do
    for child in children(path)
      save_path(child)
    end
  end
end

function save_path(path::StaticPath)
  mkpath(name(path))
  cd(name(path)) do
    for child in children(path)
      save_path(child)
    end
  end
end

# FIXME: Move old file away if it exists
function save_path(path::DynamicPath)
  try
    jldopen(name(path) * ".jld2", "w+") do f
      write(f, "DATA", getdata(path))
    end
  catch err
    log_err("Failed saving $(name(path)).jld2: $err")
  end
end

function load_workspace()
  path = rootpath()
  cd(BENTO_WORKSPACE) do
    load_paths(path)
  end
end

function load_paths(path)
  for loc in readdir()
    if isdir(loc)
      child = StaticPath(loc)
      push!(path, child)
      cd(loc) do
        load_paths(child)
      end
    elseif isfile(loc) && endswith(loc, ".jld2")
      try
        jldopen(loc, "r") do f
          path[rsplit(loc, ".jld2")[1]] = read(f, "DATA")
        end
      catch err
        log_err("Failed loading $loc: $err")
      end
    end
  end
end
