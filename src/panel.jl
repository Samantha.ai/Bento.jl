# TODO: Is this considered type piracy?
const Panel = Matrix{Cell}
const VecPanel = Vector{Cell}

function resize_panel(panel::Panel, new_size::Tuple{Int,Int}, fill_value=Cell(' '))
  old_size = size(panel)
  old_size == new_size && return panel
  new_panel = fill(fill_value, new_size)
  @static if VERSION < v"0.7-"
    shared_region = CartesianRange(CartesianIndex((1,1)), CartesianIndex(min(new_size,old_size)))
  else
    shared_region = CartesianIndices(min.(old_size, new_size))
  end
  copy!(new_panel, shared_region, panel, shared_region)
end

struct PanelIO <: IO
  screen::ScreenEmulator
end
PanelIO(height, width) = PanelIO(ScreenEmulator(width, height))
function Panel(io::PanelIO)
  panel = Panel(undef, size(io)...)
  for h in 1:size(io)[1]
    for w in 1:size(io)[2]
      panel[h,w] = io[h,w]
    end
  end
  panel
end

function modified_parse(str::String)
  retstr = ""
  for char in str
    if char == '\n'
      retstr *= '\r'
    end
    retstr *= char
  end
  retstr
end
# TODO: Arggg, matey!
Base.length(line::VT100.Line) = length(line.data)
width(pio::PanelIO) = pio.screen.ViewPortSize.width
height(pio::PanelIO) = pio.screen.ViewPortSize.height
Base.size(pio::PanelIO) = (height(pio),width(pio))
function Base.getindex(pio::PanelIO, row::Int, col::Int)
  se = pio.screen
  height, width = size(pio)
  @assert 1 <= row <= height "Invalid row index: $row"
  @assert 1 <= col <= width "Invalid col index: $col"
  #if length(se.lines) >= height - (row-1)
  if length(se.lines) >= row
    #if length(se.lines[end-(height-row)]) >= col
    if length(se.lines[row]) >= col
      #info("Getindex: ($row,$col) $(length(se.lines[end-(height-row)]))")
      #return se.lines[end-(height-row)][col]
      return se.lines[row][col]
    else
      #info("C Getindex: ($row,$col) $(length(se.lines[end-(height-row)]))")
    end
  end
  return VT100.Cell(' ')
end
function Base.print(io::PanelIO, x::Union{SubString{String}, String})
  iob = IOBuffer(modified_parse(string(x)))
  parseall!(io.screen, iob)
end
# TODO: Make this efficient!
function Base.print(io::PanelIO, panel::Union{Panel,VecPanel})
  iob = IOBuffer()
  for cell in panel
    cell_dump!(iob, cell)
  end
  print(io, String(take!(iob)))
end
Base.write(io::PanelIO, x::UInt8) = (parseall!(io.screen, IOBuffer(modified_parse(string(Char(x))))); 1)
#Base.write(io::PanelIO, x::Char) = parseall!(io.screen, IOBuffer(modified_parse(string(x))))
#Base.show_default(io::PanelIO, x) = parseall!(io.screen, IOBuffer(modified_parse(string(x))))

Base.show(io::PanelIO, apath::AbstractPath) = print(io, getdata(apath))
Base.show(io::PanelIO, x::AbstractDict{K,V}) where {K,V} = print(io, "[Dict $K => $V]")


### Printing utilities

# Defaults: Single, full-width line, white fg, black bg, no styling
# FIXME: Actually implement kwargs
function printpanel(io::PanelIO, args...; width=nothing, height=nothing, fg=nothing, bg=nothing, align=:left, fill=' ')
  panel_size = size(io)
  iob = IOBuffer()

  fstr = "$(align == :left ? '<' : '>')$(width)s"
  fspec = FormatSpec(fstr)
  for arg in args
    printfmt(iob, fspec, arg)
    # TODO: No newline on last arg? Maybe a :trailnl kwarg is necessary
    height !== nothing && println(iob)
  end

  print(io, iob)
end
