struct Button
  action::Function
  text::String
end

mutable struct ToggleSwitch
  pathref::PathRef
end
ToggleSwitch(action, text1, text2) = ToggleSwitch(action, text1, text2, false)
function process_packet!()
  str = name(tgl.pathref)
  value = lookup(tgl.pathref)
  len = length(tgl.pathref)
  print(io, "\u2554")
  for i in 1:len
    print(io, "\u2550")
  end
  print(io, "\u2557\n")
  print(io, "\u2551")
  # TODO: Highlight based on value
  print(io, str)
  print(io, "\u2551\n")
  print(io, "\u255a")
  for i in 1:len
    print(io, "\u2550")
  end
  print(io, "\u255d")
end


struct NumericSelector
  pr::PathRef
  temp_value::Number
  kb::KeyBindings
end
NumericSelector(apath::AbstractPath, kb) = NumericSelector(PathRef(fullpath(apath)), 0, kb)
