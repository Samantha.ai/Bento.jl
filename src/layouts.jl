export BlankLayout, BorderedLayout, BoardLayout, SplitLayout

struct AddWidgetRequest <: UIEvent
  widget::UIWidget
end

abstract type AbstractLayout end
struct BlankLayout <: AbstractLayout end

struct BorderedLayout <: AbstractLayout
  child
  border_cell::Cell
end
BorderedLayout(child) = BorderedLayout(child, Cell(' '))

process_packet!(self::UIWidget{BorderedLayout}, pkt) =
  send!(item(self).child, self, event(pkt))
function process_packet!(self::UIWidget{BorderedLayout}, pkt::UIPacket{DrawRequest})
  bl = item(self)

  # Ensure panel size is large enough
  if !all(event(pkt).panel_size .> (2,2))
    return reply!(req_pkt, self, DrawReply(fill(Cell(' '), event(pkt).panel_size...)))
  end

  send!(item(self).child, self, DrawRequest(event(pkt).panel_size.-2))
  reply_pkt = waithook!(self) do reply_pkt
    #reply_pkt.session == pkt.session
    event(reply_pkt) isa DrawReply
  end
  subpanel = event(reply_pkt).panel

  panel_size = event(pkt).panel_size
  panel = fill(Cell(' '), panel_size...)

  # Top and Bottom
  panel[1,:] = fill(Cell('━'), panel_size[2])
  panel[end,:] = fill(Cell('━'), panel_size[2])
  # Left and Right
  panel[:,1] = fill(Cell('┃'), panel_size[1])
  panel[:,end] = fill(Cell('┃'), panel_size[1])
  # Top-Left
  panel[1,1] = Cell('┏')
  # Top-Right
  panel[1,end] = Cell('┓')
  # Bottom-Left
  panel[end,1] = Cell('┗')
  # Bottom-Right
  panel[end,end] = Cell('┛')

  # Render item
  subSize = min.(size(panel).-2, size(subpanel))
  @static if VERSION < v"0.7-"
    srcCR = CartesianRange(CartesianIndex((1,1)), CartesianIndex(subSize))
    dstCR = CartesianRange(CartesianIndex((2,2)), CartesianIndex(subSize.+1))
    copy!(panel, dstCR, subpanel, srcCR)
  else
    srcCR = CartesianIndices((1:subSize[1], 1:subSize[2]))
    dstCR = CartesianIndices((2:subSize[1]+1, 2:subSize[2]+1))
    # FIXME: Why doesn't this work?
    copy!(panel, dstCR, subpanel, srcCR)
  end
  reply!(pkt, self, DrawReply(panel))
end
mutable struct BoardLayout <: AbstractLayout
  children::Dict{Int,UIWidget}
  locations::Dict{Int,Tuple{Int,Int}}
  sizes::Dict{Int,Tuple{Int,Int}}
  focused::Bool
  selected::Int
  kb::KeyBindings
end
BoardLayout() =
  BoardLayout(
    Dict{Int,UIWidget}(),
    Dict{Int,Tuple{Int,Int}}(),
    Dict{Int,Tuple{Int,Int}}(),
    false,
    0,
    KeyBindings(default_keybindings(BoardLayout)))
function BoardLayout(children)
  board = BoardLayout()
  add_widgets!(board, children)
  board
end

default_keybindings(::Type{BoardLayout}) = Dict{Char,Union{Dict,Symbol}}(
  '\n' => :select,
  '\b' => :delete,
  'w' => :up,
  's' => :down,
  'd' => :right,
  'a' => :left,
  'W' => :size_up,
  'S' => :size_down,
  'D' => :size_right,
  'A' => :size_left,
  '\t' => :next,
  '\e' => Dict('[' => Dict(
    'A' => :up,
    'B' => :down,
    'C' => :right,
    'D' => :left,
    'Z' => :prev,
    '1' => Dict(';' => Dict('2' => Dict(
      'A' => :size_up,
      'B' => :size_down,
      'C' => :size_right,
      'D' => :size_left,
    )))
  ))
)
function process_packet!(self::UIWidget{BoardLayout}, pkt)
  board = item(self)
  #if !board.focused
  send!(board.children[board.selected], self, event(pkt))
  #end
end

function process_action!(self::UIWidget{BoardLayout}, action::Tuple{V}) where V<:Union{Val{:up},Val{:down},Val{:left},Val{:right}}
  board = widget_item(self)
  action = first(typeof(first(action)).parameters)
  move_board(board, action)
  redraw!(self)
end
function process_action!(self::UIWidget{BoardLayout}, action::Tuple{V}) where V<:Union{Val{:size_up},Val{:size_down},Val{:size_left},Val{:size_right}}
  board = widget_item(self)
  action = first(typeof(first(action)).parameters)
  resize_board(board, action)
  redraw!(self)
end
function process_action!(self::UIWidget{BoardLayout}, action::Tuple{Val{:select}})
  board = widget_item(self)
  focus_transfer!(self, board.children[board.selected])
  redraw!(self)
end
function process_action!(self::UIWidget{BoardLayout}, action::Tuple{Val{:next}})
  board = widget_item(self)
  child_keys = collect(keys(board.children))
  selected_idx = findfirst(k->k==board.selected, child_keys) + 1
  # TODO: Is this still needed?
  @assert selected_idx != 0
  board.selected = child_keys[selected_idx>length(child_keys) ? 1 : selected_idx]
  redraw!(self)
end
function process_action!(self::UIWidget{BoardLayout}, action::Tuple{Val{:prev}})
  board = widget_item(self)
  child_keys = collect(keys(board.children))
  selected_idx = findfirst(k->k==board.selected, child_keys) - 1
  board.selected = child_keys[selected_idx==0 ? length(child_keys) : selected_idx]
  redraw!(self)
end
function move_board(board::BoardLayout, dir::Symbol)
  prev_loc = board.locations[board.selected]
  if dir == :up
    board.locations[board.selected] = prev_loc .- (1,0)
  elseif dir == :down
    board.locations[board.selected] = prev_loc .+ (1,0)
  elseif dir == :right
    board.locations[board.selected] = prev_loc .+ (0,1)
  elseif dir == :left
    board.locations[board.selected] = prev_loc .- (0,1)
  end
end
function resize_board(board::BoardLayout, dir::Symbol)
  prev_size = board.sizes[board.selected]
  if dir == :size_up
    prev_size[1] == 1 && return
    board.sizes[board.selected] = prev_size .- (1,0)
  elseif dir == :size_down
    board.sizes[board.selected] = prev_size .+ (1,0)
  elseif dir == :size_right
    board.sizes[board.selected] = prev_size .+ (0,1)
  elseif dir == :size_left
    prev_size[2] == 1 && return
    board.sizes[board.selected] = prev_size .- (0,1)
  end
end
function process_packet!(self::UIWidget{BoardLayout}, pkt::UIPacket{DrawRequest})
  board = item(self)

  compositor = Compositor(self, event(pkt))
  panel_size = event(pkt).panel_size
  for child in filter(c->board.selected!=c, keys(board.children))
    composite!(compositor, self, board.children[child]; params=Dict(
      :location => board.locations[child],
      :size => board.sizes[child],
    ))
  end
  composite!(compositor, self, board.children[board.selected]; params=Dict(
    :location => board.locations[board.selected],
    :size => board.sizes[board.selected],
  ))
  panel = Panel(compositor)
  return reply!(pkt, self, DrawReply(panel))
end
function process_packet!(self::UIWidget{BoardLayout}, pkt::UIPacket{FocusGrant})
  board = item(self)
  board.focused = true
end
function process_packet!(self::UIWidget{BoardLayout}, pkt::UIPacket{AddWidgetRequest})
  board = item(self)
  add_widget!(board, (event(pkt).widget, (1,1), (20,40)))
  send!(self, RedrawEvent())
end
function add_widgets!(board::BoardLayout, children::Vector)
  for child in children
    add_widget!(board, child)
  end
end
function add_widget!(board::BoardLayout, child::UIWidget)
  add_widget!(board, (child, (1,1), (20,40)))
end
function add_widget!(board::BoardLayout, child::Tuple{UIWidget,Tuple{Int,Int},Tuple{Int,Int}})
  item, location, size = child
  id = rand(Int)
  board.children[id] = item
  board.locations[id] = location
  board.sizes[id] = size
  if board.selected == 0
    board.selected = id
  end
end

mutable struct SplitLayout{orientation} <: AbstractLayout
  children::Vector
  portions::Vector{Int}
  focused::Bool
  compositor::UIWidget{Compositor}
end
SplitLayout{O}(children=[]; compositor=UIWidget(Compositor())) where O =
  SplitLayout{O}(children, ones(Int, length(children)), false, compositor)
const VerticalLayout = SplitLayout{:vertical}
const HorizontalLayout = SplitLayout{:horizontal}

function process_packet!(self::UIWidget{SplitLayout{O} where O}, pkt::UIPacket{<:InputEvent})
  layout = item(self)

  #if layout.focused
  #  redraw = true
  #end

  send!.(layout.children, Ref(self), Ref(event(pkt)))
end
function process_packet!(self::UIWidget{SplitLayout{O}}, pkt::UIPacket{DrawRequest}) where O
  layout = widget_item(self)

  panel_size = event(pkt).panel_size
  max_size = sum(layout.portions)
  sizes = Tuple{Int,Int}[]
  locations = Tuple{Int,Int}[]
  current_location = (1,1)
  slack = (O == :vertical ? panel_size[1] : panel_size[2])
  for (idx,portion) in enumerate(layout.portions)
    computed_portion = portion / max_size
    if O == :vertical
      if idx == length(layout.portions)
        sz = (ceil(Int, computed_portion*panel_size[1]), panel_size[2])
      else
        sz = (floor(Int, computed_portion*panel_size[1]), panel_size[2])
      end
      push!(sizes, sz)
      slack -= sz[1]
      push!(locations, current_location)
      current_location = current_location .+ (sz[1], 0)
    else
      if idx == length(layout.portions)
        sz = (panel_size[1], ceil(Int, computed_portion*panel_size[2]))
      else
        sz = (panel_size[1], floor(Int, computed_portion*panel_size[2]))
      end
      push!(sizes, sz)
      slack -= sz[2]
      push!(locations, current_location)
      current_location = current_location .+ (0, sz[2])
    end
  end
  # TODO: This could be optimized much better
  idx = 1
  while slack > 0
    if O == :vertical
      sizes[idx] = sizes[idx] .+ (1,0)
      for lidx in idx+1:length(locations)
        locations[lidx] = locations[lidx] .+ (1,0)
      end
    else
      sizes[idx] = sizes[idx] .+ (0,1)
      for lidx in idx+1:length(locations)
        locations[lidx] = locations[lidx] .+ (0,1)
      end
    end
    slack -= 1
    idx = idx == length(sizes) ? 1 : idx+1
  end

  send!(layout.compositor, self, CompositedDrawRequest(panel_size, [
   (child, Dict(
     :location => locations[idx],
     :size => sizes[idx],
   )) for (idx,child) in enumerate(layout.children)
  ]))
  reply_pkt = waithook!(self) do reply_pkt
    event(reply_pkt) isa DrawReply
  end
  panel = event(reply_pkt).panel
  return reply!(pkt, self, DrawReply(panel))
end
function process_packet!(self::UIWidget{SplitLayout{O} where O}, pkt::UIPacket{FocusGrant})
  layout = item(self)
  layout.focused = true
end
function process_packet!(self::UIWidget{SplitLayout{O} where O}, pkt::UIPacket{AddWidgetRequest})
  layout = item(self)
  push!(layout.children, event(pkt).widget)
  push!(layout.portions, 1)
  send!(self, RedrawEvent())
end
