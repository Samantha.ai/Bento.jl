struct Buffer{T}
  value::T
end

global const BENTO_BUFFERS = Dict{UUID, Buffer}()

get_buffer(uuid::UUID) = get(BENTO_BUFFERS, uuid, nothing)
function add_buffer(value)
  uuid = uuid4()
  BENTO_BUFFERS[uuid] = value
  uuid
end
del_buffer(uuid::UUID) = delete!(BENTO_BUFFERS, uuid)
