abstract type AbstractEditor end
struct JuliaDefaultEditor <: AbstractEditor end
struct OSEditor <: AbstractEditor
  edit_str::String # Name of editor to launch
end
struct WidgetEditor <: AbstractEditor
  widget
end

doedit(editors::Dict{Type,AbstractEditor}) = _doedit(editors[Any])
function doedit(editors::Dict{Type,AbstractEditor}, x::T) where T
  closestType = Any
  for editorType in keys(editors)
    if T <: editorType <: closestType
      closestType = editorType
    end
  end
  _doedit(editors[closestType], x)
end
function editor_preload(io, x)
  println(io, repr(x))
  flush(io)
  seekstart(io)
end
_doedit(editor::JuliaDefaultEditor, x) = mktemp() do path, io
  editor_preload(io, x)
  edit(path)
  Main.eval(parse(readstring(io)))
end
_doedit(editor::JuliaDefaultEditor) = mktemp() do path, io
  edit(path)
  Main.eval(parse(readstring(io)))
end
#=
_doedit(editor::OSEditor, x) = mktemp() do path, io
  println(io, repr(x)); flush(io)
  seekstart(io)
  run(`$(editor.edit_str) $path`)
  parse(readstring(io))
end
=#
