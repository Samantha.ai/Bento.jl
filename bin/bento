#!/usr/bin/env julia

using Pkg
Pkg.API.activate(joinpath(@__DIR__, ".."))

using ArgParse

s = ArgParseSettings()
@add_arg_table s begin
  "--quiet", "-q"
    help = "don't do anything with the terminal"
    action = :store_true
  "--bare", "-b"
    help = "don't load/save from/to the workspace"
    action = :store_true
end
config = parse_args(ARGS, s)

println("Loading bento...")
using Bento

init_bento(;quiet=config["quiet"],bare=config["bare"])

func = load_bentorc()

if !config["bare"]
  if Bento.BENTO_WORKSPACE[] != "" && length(readdir(Bento.BENTO_WORKSPACE[])) > 0
    load_workspace()
  end
  if Bento.BENTO_WORKSPACE_AUTOSAVE[]
    atexit() do
      save_workspace()
    end
  end
end

tui = UIWidget(TerminalUI())
set_default_widget!(tui)
func(tui)
start_ui(tui)

wait()
