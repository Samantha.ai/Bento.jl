@testset "Widgets" begin
  mutable struct TestWidget
    counter::Int
  end

  menu = UIWidget(Menu())
  bl = UIWidget(BorderedLayout(menu))
  anchor = UIWidget(TestWidget(0))

  Bento.process_packet!(self::UIWidget{TestWidget}, pkt) =
    item(self).counter += 1

  for i in 1:3
    Bento.send!(bl, anchor, Bento.DrawRequest((10,10)))
    yield()
  end
  sleep(1)

  @test length(bl.replybox) == 0
  @test length(bl.mappings) == 0

  @test length(menu.replybox) == 0
  @test length(menu.mappings) == 0

  @test item(anchor).counter == 3
end
