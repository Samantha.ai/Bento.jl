struct TestInputSource <: Bento.InputSource end

@testset "KeyBindings" begin
  kb = KeyBindings(KBDict(
    'a' => :a,
    'b' => KBDict(
      'c' => KBParseNumber(KBDict(
        'd' => :d,
      )),
    ),
  ))

  for str_pairs in [
      "a" => (:a,),
      "bc123d" => (123, :d)
    ]
    str, expected = str_pairs
    println("Testing $str and expecting $expected")

    result = nothing
    for (idx,char) in enumerate(str)
      result = Bento.process_input(kb, Bento.InputEvent(TestInputSource(), char))
      idx != length(str) && @assert result == nothing "Result was $result"
    end
    println("Result was $result")
    @test length(result) == length(expected) && all(result .== expected)
    Bento.clear_state!(kb)
  end
end
